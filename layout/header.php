<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="../style_sheets/nav_style.css">
	<link rel="stylesheet" href="../style_sheets/general_style.css">
</head>
<body>
	<?php 
	include_once('../db.php');
	session_start();
	$categorias = $modelos_categorias->Todos();
	?>
	<nav class="navbar navbar-default navbar-inverse" role="navigation">
	  <div class="container-fluid">
	    <!-- mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand active" href="../layout/">Home</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
		<?php
			if (isset($_SESSION['current_email'])) {
				echo '<li><a href="../layout/articulos_todos.php">Productos</a></li>';
				echo '<li class="dropdown">';
					echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">Categorias<span class="caret"></span></a>';
					echo '<ul class="dropdown-menu" role="menu">';
					while ($row = $sql->nextResultRow($categorias)) {
						echo "<li><a href='../layout/articulos_categoria.php?id= " . $row['id'] . "'> " . $row['descripcion'] . "</a></li>";
					}	
					echo '</ul>';
				echo '</li>';
			}
	      	
	          
	        

		?>
	    <?php
			if (isset($_SESSION['current_email'])) {
				if ($_SESSION['current_email'] == "admin@admin.com" || $_SESSION['current_email']== "admin@some.com") {
					echo '<li class="dropdown">';
					echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">Mantenimiento<span class="caret"></span></a>';
					echo     '<ul class="dropdown-menu" role="menu">';
					echo         '<li><a href="../usuarios">Usuarios</a></li>';
					echo         '<li class="divider"></li>';
					echo         '<li><a href="../categorias">Categorias</a></li>';
					echo         '<li class="divider"></li>';
					echo         '<li><a href="../articulos">Productos</a></li>';
					echo     '</ul>';
					echo '</li>';
				}
			}
		?>
	      </ul>
	      <form  class="navbar-form navbar-left" role="search" >
	        <div class="form-group">
	          <input type="text" class="form-control" placeholder="Buscar">
	        </div>
	        <button type="submit" class="btn btn-default">Buscar</button>
	      </form>
	<?php
		if (!isset($_SESSION['current_user']) && !isset($_SESSION['current_email']) ) {
		echo '<ul class= "nav navbar-nav navbar-right">';
	    echo   '<li><p class=""navbar-text"">Already have an account?</p></li>';
	    echo    '<li class="dropdown">';
	    echo     '<a href="#" class="dropdown-toggle" data-toggle="dropdown" ><b>Login</b> <span class="caret"></span></a>';
		echo		'<ul id="login-dp" class="dropdown-menu">';
		echo			'<li>';
		echo				 '<div class="row">';
		echo						'<div class="col-md-12">';
		echo							'Login via';
		echo							'<div class="social-buttons">';
		echo								'<a href="/" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>';
		echo								'<a href="/" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>';
		echo							'</div>';
	    echo                       'or';
		echo							 '<form method="POST" action="../user/verificar_login.php" class="form" role="form" accept-charset="UTF-8" id="login-nav">';
		echo									'<div class="form-group">';
		echo										 '<label class="sr-only" for="exampleInputEmail2">Email address</label>';
		echo										 '<input type="email" class="form-control" name="usr" id="usr-email" placeholder="Email address" required>';
		echo									'</div>';
		echo									'<div class="form-group">';
		echo										 '<label class="sr-only" for="exampleInputPassword2">Password</label>';
		echo										 '<input type="password" class="form-control" name="pwd" id="usr-pwd" placeholder="Password" required>';
	    echo                                         '<div class="help-block text-right"><a href="">Forget the password ?</a></div>';
		echo									'</div>';
		echo									'<div class="form-group">';
		echo										 '<button type="submit" class="btn btn-primary btn-block">Sign in</button>';
		echo									'</div>';
		echo									'<div class="checkbox">';
		echo										 '<label>';
		echo										 '<input type="checkbox"> keep me logged-in';
		echo										 '</label>';
		echo									'</div>';
		echo							 '</form>';
		echo						'</div>';
		echo						'<div class="bottom text-center">';
		echo							'New here ? <a href="../usuarios/create.php"><b>Join Us</b></a>';
		echo						'</div>';
		echo				 '</div>';
		echo			'</li>';
		echo		'</ul>';
	    echo   '</li>';
	    echo  '</ul>';
		}else{
		echo	'<div class="pull-right">';
        echo        '<ul class="nav pull-right">';
        echo            '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, ' . $_SESSION['current_user']. '<b class="caret"></b></a>';
        echo                '<ul class="dropdown-menu">';
        echo                    '<li><a href="/user/preferences"><i class="icon-cog"></i> Preferences</a></li>';
        echo                    '<li><a href="../carrito"><i class="icon-envelope"></i> Carrito</a></li>';
        echo                    '<li class="divider"></li>';
        echo                    '<li><a href="../user/logout.php"><i class="icon-off"></i> Logout</a></li>';
        echo                '</ul>';
        echo            '</li>';
        echo        '</ul>';
        echo      '</div>';
		}
	?>
	
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>