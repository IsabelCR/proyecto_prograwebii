<?php 
include_once '../layout/header.php';
include_once('../db.php');
$resultado = $sql->runSql('select * from producto');

echo '<table class="tbl_producto" border = 1>';
while ($row = $sql->nextResultRow($resultado)) {
    echo '<tr>';
        echo "<td class='descripcion'>" . $row['descripcion'] . "</td>";
        echo "<td class='precio'> $" . $row['precio'] . "</td>";
        echo "<td class='foto'> <img src = '" . $row['foto'] . "' id = 'img'></img></td>";
        echo "<td>
                <a href = '../carrito/add.php?id=" . $row['id'] . "'><img src='../images/add-to-cart-button.png' id = 'add'></a>
            </td>";                  
    echo '</tr>';
}
echo '</table>';

include_once '../layout/footer.php';
?>