<?php
session_start();
$usr = $_POST['usr'];
$pass = $_POST['pass'];
$nombre = $_POST['nombre'];
$accion = $_POST['accion'];

include_once('../db.php');


if (!$usr == "" && !$pass == "" && !$nombre == "") {

	if ($accion == 'insert'){
		$modelos_usuarios->Insert($usr, $nombre, $pass);
	} else {
		$modelos_usuarios->Update($usr, $nombre, $pass);	
	}
}

if ($_SESSION['current_email'] == "admin@admin.com" || $_SESSION['current_email']== "admin@some.com") {
	Utils::Redirect('usuarios');
}else{
	Utils::Redirect('layout');
}

include_once '../layout/footer.php';