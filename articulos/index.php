<?php 
include_once '../layout/header.php'; 
include_once('../db.php');
$resultado = $sql->runSql('select * from producto');

echo '<a href="../articulos/create.php"><img src="../images/Button-Add-3-512.png" id = "add"></a>';
echo '<br/>';
echo '<table class = "tbl_producto" border="1">';
echo	'<tr>';
echo		'<th>Producto</th>';
echo		'<th>Precio</th>';
echo		'<th>Categoria</th>';
echo		'<th>Imagen</th>';
echo		'<th>Acciones</th>';
echo	'</tr>';	
	while ($row = $sql->nextResultRow($resultado)) {
		$categoria = $modelos_categorias->CategoriaById($row['id_categoria']);
    	echo '<tr>';
    		echo "<td>" . $row['descripcion'] . "</td>";
    		echo "<td>$" . $row['precio'] . "</td>";
    		echo "<td >" . $categoria['descripcion']. "</td>";
    		echo "<td> <img src = '" . $row['foto'] . "' id = 'img'></img></td>";
    		echo "<td>
    				<a href= ../articulos/edit.php?id=" . $row['id'] . ">Editar</a>
    				<br/>
    				<a href= ../articulos/delete.php?id=" . $row['id'] . ">Eliminar</a>
    			  </td>";
    	echo '</tr>';
	}
include_once '../layout/footer.php';
?>