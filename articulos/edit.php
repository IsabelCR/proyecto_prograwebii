<?php 
include_once '../layout/header.php'; 
include_once('../db.php');

$articulo = $modelo_articulos->ArticuloById($_GET['id']);
$categorias = $modelos_categorias->Todos();
?>

<form id = "create_form" method="POST" action="../articulos/almacenar.php">
	<label for="descripcion">Descripcion</label>
	<input name="descripcion" value="<?=$articulo['descripcion']?>">
	<br/>
	<br/>
	<label for="precio">Precio</label>
	<input name="precio" value="<?=$articulo['precio']?>" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
	<br/>
	<br/>
	<label for="categoria">Categoria </label>
	<select name="id_categoria">
		<?php
			while ($row = $sql->nextResultRow($categorias)) {
				$selected = ($row['id'] == $articulo['id_categoria']) ? 'selected' : '';
				echo "<option value=". $row['id'] ."  ". $selected ." > " . $row['descripcion'] . "</option>";
			}
		?>
	</select>
	<br/>
	<br/>
	<a href="../articulos">Atrás</a>
	<input id="unirse" type="submit">
	<input type="hidden" name="id" value="<?=$articulo['id']?>">
	<input type="hidden" name="foto" value="<?=$articulo['foto']?>">
	<input type="hidden" name="accion" value="update">
</form>
<?php include_once '../layout/footer.php'; ?>