<?php 
include_once '../layout/header.php'; 
include_once('../db.php');
$categorias = $modelos_categorias->Todos();
?>

<form method="POST" action="../articulos/almacenar.php"  enctype = "multipart/form-data" id = "create_form">
	<label for="descripcion">Nombre del articulo </label>
	<input name="descripcion">
	<br/>
	<label for="precio">Precio</label>
	<input name="precio" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
	<br/>
	<label for="foto">Foto</label>
	<input type="file" name="foto">
	<br/>
	<select name="id_categoria">
		<?php
			while ($row = $sql->nextResultRow($categorias)) {
				echo "<option value=". $row['id'] .">" . $row['descripcion'] . "</option>";
			}
		?>
	</select>
	<a href="../articulos/index.php">Atrás</a>
	<input type="submit">
	<input type="hidden" name="accion" value="insert">
	
</form>

<?php
include_once '../layout/footer.php';
?>