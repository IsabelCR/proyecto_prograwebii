<?php

include_once('../db/sql_abstract.php');
include_once('../db/sql_postgres.php');
include_once('../modelos/categorias.php');
include_once('../modelos/usuarios.php');
include_once('../modelos/articulos.php');
include_once('../modelos/carrito.php');
include_once('../modelos/facturas.php');
include_once('../lib/util.php');

$server       = 'localhost';
$database     = 'e_shop';
$user         = 'postgres';
$password     = '12345';
$driver_class = 'DB\SqlPostgres';
//$driver_class = 'DB\SqlMySql';

$sql = new $driver_class($server, $database, $user, $password);
$sql->connect();

$modelos_categorias = new Categoria($sql);
$modelos_usuarios = new Usuarios($sql);
$modelo_articulos = new Articulos($sql);
$modelos_carritos = new Carrito($sql);
$modelos_factura = new Factura($sql);