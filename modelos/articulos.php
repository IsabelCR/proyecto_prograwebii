<?php
	class Articulos
	{
		private $sql;
		function __construct($sql)
		{
			$this->sql = $sql;	
		}

		public function ArticuloById($id)
		{
			$query = "select * from producto where id = '" . $id . "'";
			$resultado = $this->sql->runSql($query);
			$producto = $this->sql->nextResultRow($resultado);
			if ($producto == null){
				die('<h1>Articulo no encontrado</h1>');
			}
		return $producto;
		}

		public function ArticuloByCategoria($id_categoria)
		{
			$query = "select * from producto where id_categoria = '" . $id_categoria . "'";
			$resultado = $this->sql->runSql($query);
			$producto = $this->sql->nextResultRow($resultado);
			if ($producto == null){
				die('<h1>Articulo no encontrado</h1>');
			}
		return $producto;
		}


		public function Insert($descripcion, $foto, $precio, $id_categoria)
		{
			$this->sql->runSql("insert into producto (descripcion, foto, precio, id_categoria) values ('" . $descripcion . "','" . $foto . "', '" . $precio . "','".$id_categoria."' )");
		}

		public function Update($id, $descripcion, $foto, $precio, $id_categoria)
		{
			$this->sql->runSql("update producto set descripcion='" . $descripcion . "', foto = '" . $foto . "', precio = '".$precio."', id_categoria = '".$id_categoria."' where id = '" . $id . "'");
		}

		public function Delete($id)
		{
			$this->sql->runSql("delete from producto where id = '" . $id . "'");
		}
	}
?>