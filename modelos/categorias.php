<?php
	class Categoria
	{
		private $sql;
		function __construct($sql)
		{
			$this->sql = $sql;	
		}

		public function CategoriaById($id)
		{
			$query = 'select id, descripcion 
			from categoria where id = ' . $id;
			$resultado = $this->sql->runSql($query);
			$categoria = $this->sql->nextResultRow($resultado);
			if ($categoria == null){
				die('<h1>Categoría No Encontrada</h1>');
			}
		return $categoria;
		}

		public function Insert($descripcion)
		{
			$this->sql->runSql("insert into categoria (descripcion) values ('" . $descripcion . "')");
		}

		public function Todos()
		{
			$query = 'select id, descripcion from categoria';
			$resultado = $this->sql->runSql($query);
			return $resultado;
		}

		public function Update($id, $descripcion)
		{
			$this->sql->runSql("update categoria set descripcion='" . $descripcion . "' where id =" . $id);
		}

		public function Delete($id)
		{
			$this->sql->runSql("delete from categoria where id = " . $id);
		}
	}
?>